FROM cm2network/tf2:base

ENV METAMOD_VERSION 1.10
ENV SOURCEMOD_VERSION 1.10

USER root
RUN set -x \
        && apt-get update \
		&& apt-get install rsync -y
		
USER steam

RUN set -x \
        && LATESTMM=$(wget -qO- https://mms.alliedmods.net/mmsdrop/"${METAMOD_VERSION}"/mmsource-latest-linux) \
        && LATESTSM=$(wget -qO- https://sm.alliedmods.net/smdrop/"${SOURCEMOD_VERSION}"/sourcemod-latest-linux) \
        && wget -qO- https://mms.alliedmods.net/mmsdrop/"${METAMOD_VERSION}"/"${LATESTMM}" | tar xvzf - -C "${STEAMAPPDIR}/${STEAMAPP}" \
        && wget -qO- https://sm.alliedmods.net/smdrop/"${SOURCEMOD_VERSION}"/"${LATESTSM}" | tar xvzf - -C "${STEAMAPPDIR}/${STEAMAPP}"
		
ADD MGEMod "${STEAMAPPDIR}/${STEAMAPP}"/MGEMod

RUN set -x \
		&& rsync -a "${STEAMAPPDIR}/${STEAMAPP}"/MGEMod/addons "${STEAMAPPDIR}/${STEAMAPP}" \
		&& rsync -a "${STEAMAPPDIR}/${STEAMAPP}"/MGEMod/maps "${STEAMAPPDIR}/${STEAMAPP}" 
		

# All other properties are inherited from the base image
